#!/usr/bin/ruby
# encoding: UTF-8
begin
require "rubygems"
require "mechanize"
require "iconv"


#  Extend File class to support tail method
class File

  def tail(n)
    buffer = 1024
    idx = (size - buffer).abs
    chunks = []
    lines = 0

    begin
      seek(idx)
      chunk = read(buffer)
      lines += chunk.count("\n")
      chunks.unshift chunk
      idx -= buffer
    end while lines < ( n + 1 ) && pos != 0

    tail_of_file = chunks.join('')
    ary = tail_of_file.split(/\n/)
    lines_to_return = ary[ ary.size - n, ary.size - 1 ]

  end
end

#  Check for comand-line args

url = ARGV[0]
@max_tries = ARGV[1].to_i
@sleep_till_next_grab = ARGV[2].to_i
@skip_to = ARGV[3]

#  Continue work from the last logged action, or start new

if not @skip_to.nil?
  file = File.open('grabber.log','r')
  @skip_to = file.tail(1).first
  @skip_to = Iconv.conv('utf-8', "utf-8", @skip_to)
  file.close
  puts @skip_to
else
  @skip_to = nil
end

def log text
  log = File.open 'grabber.log','a'
  log.write text
  log.close
end

@time = Time.new

log "\n_______________________________________\nSTART AT: #{@time}\n"
sleep(1)

@agent = Mechanize.new
@agent.user_agent_alias = 'Linux Konqueror'
@agent.pluggable_parser.default = Mechanize::Download




@files = 0
@missing_files = []

if url[/^http:\/\//].nil?
  url = 'http://' + url.to_s 
end

#  Beautifing output

def b_puts obj
  print "\n"
    5.times{print "*"}
    print "\n"
    puts obj
    5.times{print "*"}
    print "\n"
end

@links = []

# Recursive scanning/grabbing function

def rec_link_search link, dir='grabbed'
  tries = 0
  begin
    @too_long_name = false
    
    #  If link given is page find all links on that page
    
    if link.class == Mechanize::Page
      links = link.search("ul a")
      links.each do |_l|
        _l = Mechanize::Page::Link.new _l, @agent, link
        d = "grabbed/#{_l.text.gsub(%r{[\/\\'"]}, ' ')}"
        if @skip_to.nil? or not @skip_to.to_s.match(d.to_s).nil?
          log "***\n#{d}\n***\n"
          FileUtils.mkdir_p d unless File.directory?(d)
          @links = []
          rec_link_search _l, d
        else
          puts  "Skipping: #{d} - #{@skip_to.to_s.match(d.to_s).nil?}"
          next
        end
      end
    
    #  Else it is link then click and work with next page
    
    else
      page = link.click
      links = page.search("ul a")
      links.each do |_l|
        _l = Mechanize::Page::Link.new _l, @agent, page
        if @links.include? _l.href.to_s then puts "skip"; next; end
        @links << _l.href.to_s
        if _l.text != link.text
          d = "#{dir.to_s}/#{_l.text.gsub(%r{[\/\\'"]}, ' ')}"
          if not @skip_to.nil? and @skip_to.to_s.match(d.to_s).nil?;puts  "Skipping: #{d} - #{@skip_to.to_s.match(d.to_s).nil?}"; next; elsif @skip_to == d.to_s; @skip_to = nil end
          FileUtils.mkdir_p d unless File.directory?(d)
          rec_link_search _l, d
        else
          next
        end
      end
    end
    
    # Look for file links on current page

    file_links = link.class == Mechanize::Page ? link.search("td>font>a") : page.search("td>font>a")
    file_names = link.class == Mechanize::Page ? link.search("td>b>a>font") : page.search("td>b>a>font")

    #  Remember all files listed

    f_names = []
    file_names.each do |name|
      f_names << name.text
    end
  
    puts "flinks: #{file_links.length - 2}"
    puts "#{dir}\n"
    log "#{dir}\n"
    
    #  Proceed all files

    file_links.each do |file,index|
      if file.attr('href').match(/upload/)
      
        f_name_node = file.parent.parent.search("b>a>font")  #  Look for a name of current file link
      
        
        file.attr('href')[/[\w\W]+(\.[\w]+)$/]  #  Grab file extension
        f_ext =  $1.gsub(%r{[\/\\'"]}, ' ')
        
        f_names = f_names - [f_name_node.text]

        #  Get file name from node
        #  Change every ', ", or / for space
        f_name = @too_long_name ? f_name_node.text.gsub(%r{[\/\\'"]}, ' ') : f_name_node.text.gsub(%r{[\/\\'"]}, ' ')[0..100]
       
        log "\t#{f_name} link: http://igumo.ru#{file.attr('href')}\n"
        @agent.get(file.attr('href')).save("#{dir}/#{f_name}#{f_ext}")
        @files = @files + 1
        sleep(@sleep_till_next_grab)
      end
    end
    
    #  Check if there are any files unprocessed

    f_names.each do |f_name|
      puts "MISSING: #{dir}/#{f_name.gsub(%r{[\/\\'"]}, ' ')}\n"
      log "MISSING: #{dir}/#{f_name}\n"
      @missing_files << "#{dir}/#{f_name}"
    end
          
    #  Now we need to go deeper ^_^
    
    next_page = link.class == Mechanize::Page ? link.links_with(:href => %r{\/elx\/([\w\W]+\/)*(page-\d)}) : page.links_with(:href => %r{\/elx\/([\w\W]+\/)*(page-\d)})
    next_page.each do |link|
      rec_link_search link, dir
    end
    log "\n"

    #  Log and workaround errors if any

    rescue Timeout::Error
      log "TO:ERROR\n"
      if tries <= @max_tries
        sleep(10 * tries)
        tries = tries + 1
        retry
      else
        log "ABORTED.\n"
        raise
      end
    rescue SystemExit, Interrupt
      log "ABORTED.\n"
      raise
    rescue Errno::ENOENT => e
      @too_long_name = true
      log "SHORTENNED\n"
    end
end


begin
  puts "Parsing: #{url}"
  page = @agent.get(url)
  rec_link_search page

rescue SocketError => e
  puts "Something wrong with url or internet connection. Check both before try again."
  log "SOCK:ERROR\n"
  tries = tries + 1
  if tries <= @max_tries
    sleep(10 * tries)
    retry
  else
    log "ABORTED.\n"
    raise
  end
end

#  Final log message


b_puts "Files missing: #{@missing_files.length}\nProgramm has completed all work. Files downloaded: #{@files}.\n If this isn't what you expected:\n - check all params and restart."
log "\n\nPROCESSED FILES: #{@files}.\nMISSED FILES: #{@missing_files.length}\n\t#{@missing_files.join ",\n\t"}.\nIT TOOK #{Time.at(Time.new - @time).gmtime.strftime('%R:%S')} (HH:MM:SS).\nALL OK."

 rescue SystemExit, Interrupt
    log "ABORTED.\n"
    raise
  rescue Exception
    log "AHTUNG!.\n"
    raise
 end